<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="noindex, nofollow">
  <title>Laravel Utilities</title>
  <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
  <style>
    :root {
      --input-padding-x: 1.5rem;
      --input-padding-y: .75rem;
    }

    .card-signin {
      border: 0;
      border-radius: 1rem;
      box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
    }

    .card-signin .card-title {
      margin-bottom: 2rem;
      font-weight: 300;
      font-size: 1.5rem;
    }

    .card-signin .card-body {
      padding: 2rem;
    }

    .form-signin {
      width: 100%;
    }

    .form-signin .btn {
      font-size: 80%;
      border-radius: 5rem;
      letter-spacing: .1rem;
      font-weight: bold;
      padding: 1rem;
      transition: all 0.2s;
    }

    .form-label-group {
      position: relative;
      margin-bottom: 1rem;
    }

    .form-label-group input {
      height: auto;
      border-radius: 2rem;
    }

    .form-label-group>input,
    .form-label-group>label {
      padding: var(--input-padding-y) var(--input-padding-x);
    }

    .form-label-group>label {
      position: absolute;
      top: 0;
      left: 0;
      display: block;
      width: 100%;
      margin-bottom: 0;
      /* Override default `<label>` margin */
      line-height: 1.5;
      color: #495057;
      border: 1px solid transparent;
      border-radius: .25rem;
      transition: all .1s ease-in-out;
    }

    .form-label-group input::-webkit-input-placeholder {
      color: transparent;
    }

    .form-label-group input:-ms-input-placeholder {
      color: transparent;
    }

    .form-label-group input::-ms-input-placeholder {
      color: transparent;
    }

    .form-label-group input::-moz-placeholder {
      color: transparent;
    }

    .form-label-group input::placeholder {
      color: transparent;
    }

    .form-label-group input:not(:placeholder-shown) {
      padding-top: calc(var(--input-padding-y) + var(--input-padding-y) * (2 / 3));
      padding-bottom: calc(var(--input-padding-y) / 3);
    }

    .form-label-group input:not(:placeholder-shown)~label {
      padding-top: calc(var(--input-padding-y) / 3);
      padding-bottom: calc(var(--input-padding-y) / 3);
      font-size: 12px;
      color: #777;
    }

    .btn-google {
      color: white;
      background-color: #ea4335;
    }

    .btn-facebook {
      color: white;
      background-color: #3b5998;
    }
  </style>
</head>
<body>
<div class="container-fluid">
  @if(!Session::has('impersonate'))

    <div class="container">
      <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div class="card card-signin my-5">
            <div class="card-body">
              <h5 class="card-title text-center">Sign In</h5>
              <form class="form-signin" method="POST" action="{{action('UserController@impersonate')}}">
                {{ csrf_field() }}

                <div class="form-label-group">
                  <input type="text" id="inputEmail" name="username" class="form-control" placeholder="Username" required autofocus/>
                  <label for="inputEmail">Username</label>
                </div>
  
                <div class="form-label-group">
                  <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password" >
                  <label for="inputPassword">Password</label>
                </div>
  
                {{-- <div class="custom-control custom-checkbox mb-3">
                  <input type="checkbox" class="custom-control-input" id="customCheck1">
                  <label class="custom-control-label" for="customCheck1">Remember password</label>
                </div> --}}
                
                <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
                
                {{-- <hr class="my-4"> --}}

                {{-- <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i class="fab fa-google mr-2"></i> Sign in with Google</button>
                <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fab fa-facebook-f mr-2"></i> Sign in with Facebook</button> --}}
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  

  @else 

    <div>
        <h1>Welcome to Utils</h1>

        <div style="position:absolute;top:10px;right:15px;">
        <a href="{{ action('UserController@stopImpersonate') }}" class="btn btn-primary">        
            <i class="fas fa-sign-out-alt"></i>Stop Impersonate
        </a>
        </div>  

        <div style="display:flex;flex:2;">
          <ul class="list-group" style="flex:1;">
            <li class="list-group-item">
                <strong>Generate JWT Secret</strong>
                <br/>
                <i>/utils/jwt</i>
            </li>
            <li class="list-group-item">
                <strong>Compare raw password and hashed password</strong>
                <br/>
                <i>/utils/check-password?password=[password]&hashed=[hashed_password]</i>
            </li>
            <li class="list-group-item">
                <strong>Generate hashed password</strong>
                <br/>
                <i>/utils/password?password=[password]></i>
            </li>
            <li class="list-group-item">
                <strong>Queue | Retry failed jobs</strong>
                <br/>
                <i>/utils/queue/retry/{id?}</i>
            </li>
            <li class="list-group-item">
                <strong>Queue | Clear a failed job</strong>
                <br/>
                <i>/utils/queue/forget/{id}</i>
            </li>
            <li class="list-group-item">
                <strong>Queue | Get failed jobs</strong>
                <br/>
                <i>/utils/queue/failed</i>
            </li>
            <li class="list-group-item">
                <strong>Queue | Flush</strong>
                <br/>
                <i>/utils/queue/flush</i>
            </li>
            <li class="list-group-item">
                <strong>View logs</strong>
                <br/>
                <i>/utils/logs</i>
            </li>
            {{-- <li class="list-group-item">
              <strong>Maintainance mode</strong>
              <br/>
              <i>/utils/status/up</i>
              <br/>
              <i>/utils/status/down</i>
            </li> --}}
          </ul>
          <ul class="list-group" style="flex:1;">
            <li class="list-group-item">
              <strong>Place reserved</strong>
              <br/>
              <i>@todo</i>
            </li>
          </ul>
        </div>
    </div>

  @endif

  </div>
  <!-- jQuery for Bootstrap -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
          integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
          crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
          integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
          crossorigin="anonymous"></script>
  <!-- FontAwesome -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</body>
</html>
