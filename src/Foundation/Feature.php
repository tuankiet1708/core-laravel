<?php

namespace LucidApp\Foundation;

use Lucid\Foundation\Feature as LucidFeature;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

abstract class Feature extends LucidFeature
{
    /**
     * Run the given job in the given queue.
     *
     * @param string $job
     * @param array  $arguments
     * @param string $queue
     * @return mixed
     */
    public function runInQueue($job, array $arguments = [], $queue = 'default')
    {
        // instantiate and queue the job
        if ($arguments instanceof Request) {
            $jobInstance = $this->marshal($job, $arguments, []);
        } else {
            $jobInstance = $this->marshal($job, new Collection(), $arguments);
        }

        // Set queue
        $jobInstance->onQueue((string)$queue);

        return $this->dispatch($jobInstance);
    }
}
