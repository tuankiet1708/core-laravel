<?php
namespace LucidApp\Foundation;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        // Register the service providers of your Services here.
        $this->app->register(\LucidApp\Services\Core\Providers\CoreServiceProvider::class);
    }
}
