<?php

namespace LucidApp\Foundation\Exceptions;

use Illuminate\Validation\Validator as IlluminateValidator;
use Exception;
use Lucid\Foundation\InvalidInputException as LucidInvalidInputException;

/**
 * An exception class that supports validators by extracting their messages
 * when given, or an array of messages as strings.
 */
class InvalidInputException extends LucidInvalidInputException
{
    public $validations = [];

    public function __construct($message = '', $code = 412, Exception $previous = null)
    {
        if ($message instanceof IlluminateValidator) {
            $this->validations = $message->messages()->getMessages();
            $message = $message->messages()->all();
        }

        if (is_array($message)) {
            $message = implode("\n", $message);
        }

        parent::__construct($message, $code, $previous);
    }
}
