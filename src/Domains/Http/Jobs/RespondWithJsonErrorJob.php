<?php

namespace LucidApp\Domains\Http\Jobs;

use Illuminate\Routing\ResponseFactory;

class RespondWithJsonErrorJob extends RespondWithJsonJob
{
    public function __construct(
        $content = null,
        $errors = [
            [
                'message' => 'An error occurred',
                'code' => 400
            ]
        ],
        $status = 400,
        $headers = [],
        $options = 0
    ) {
        $this->content = [
            'data'   => $content,
            'statusCode' => $status,
            'status' => $status,
            'errors' => $errors
        ];

        $this->status = $status;
        $this->headers = $headers;
        $this->options = $options;
    }

    public function handle(ResponseFactory $response)
    {
        return $response->json($this->content, $this->status, $this->headers, $this->options);
    }
}
