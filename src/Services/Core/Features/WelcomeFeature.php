<?php
namespace LucidApp\Services\Core\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use LucidApp\Domains\Http\Jobs\RespondWithJsonJob;
use LucidApp\Domains\Http\Jobs\RespondWithViewJob;
use LucidApp\Domains\Http\Jobs\RespondWithJsonErrorJob;

class WelcomeFeature extends Feature
{
    public function __construct() {
        // @todo: implement here
    }

    public function handle(Request $request)
    {
        $data = 'Welcome to Lucid';

        return $this->run(new RespondWithJsonJob($data));
    }
}
