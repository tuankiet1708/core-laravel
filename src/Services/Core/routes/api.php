<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/core/v1
Route::group(['prefix' => 'core/v1', 'middleware' => ['cors', 'jwt.auth']], function() {
    Route::get('/welcome', 'WelcomeController@index');
});