<?php

namespace LucidApp\Services\Core\Http\Controllers;

use Lucid\Foundation\Http\Controller;
use LucidApp\Services\Core\Features\WelcomeFeature;

class WelcomeController extends Controller
{
    public function index() {
        return $this->serve(WelcomeFeature::class);
    }
}