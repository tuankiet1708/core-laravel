<?php 

/**
 * Array keyBy
 *
 * @param   array  $array
 * @param   string $keyPath
 * @return  array
 */
if (!function_exists('array_key_by')) {
    function array_key_by(array $array = [], $keyPath = '_id')
    {
        return array_combine(array_map('strval', array_pluck($array, $keyPath)), $array);
    }
}

/**
 * Case-insensitive in_array
 *
 * @param   string
 * @param   array
 * @return  array
 */
if (!function_exists('in_arrayi')) {
    function in_arrayi($needle, $haystack)
    {
        return in_array(strtolower($needle), array_map('strtolower', $haystack));
    }
}

/**
 * Fill values in string with defined parameters. Example:
 *
 * $string = 'I am {name}.';
 * $params = ['name' => 'Kiet'];
 * Result: 'I am Kiet.'
 *
 * @param   string $string
 * @param   array $params
 * @param   bool $skipEmptyParams
 *
 * @return  string
 * @author   kiettlt
 * @modified hieult2 Add $skipEmptyParams
 */
if (!function_exists('str_fill_values')) {
    function str_fill_values($string, array $params = [], $skipEmptyParams = false)
    {
        preg_match_all('~\{([^{}]+)\}~', $string, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            $string = str_replace($match[0], array_get($params, $match[1], $skipEmptyParams ? $match[0] : ''), $string);
        }

        return $string;
    }
}