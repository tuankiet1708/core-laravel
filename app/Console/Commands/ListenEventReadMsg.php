<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EventBus\RabbitMQ;
use DB;
use Carbon\Carbon;

class ListenEventReadMsg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eventbus:readmsg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen events from eventbus';

    /**
     * The binding key to listen.
     * More info at: https://www.rabbitmq.com/tutorials/tutorial-five-php.html
     * 
     * @var string
     */
    protected $bindingKey = 'readmsg.#';

    /**
     * The consumer tag.
     * More info at: https://www.rabbitmq.com/tutorials/tutorial-five-php.html
     * 
     * @var string
     */
    protected $consumerTag = 'readmsg';
    
    /**
     * The queue name.
     * 
     * @var string
     */
    protected $queueName = 'eventbus_core_readmsg';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        (new RabbitMQ())->consume(
            $this->bindingKey, 
            function($msg) {
                $consumer = json_decode($msg->body, true);
                $consumer['routing_key'] = $msg->delivery_info['routing_key'];
                $trace = $consumer['message'] ?? [];
                app('eventbus.publisher')->ack($trace, $consumer);

                //
                $time = Carbon::now()->getTimestamp();
                
                echo ' [x] ', $msg->delivery_info['routing_key'], ' : ', $msg->body, "\n";
                echo '     └─● $time : ', date('Y-m-d H:i:s', $time), "\n";
                echo '     └─● $msg  : ', json_encode($msg), "\n";                
                echo ' ------' . "\n";

                DB::collection('test_queue')->insert([
                    'routing_key' => $msg->delivery_info['routing_key'],
                    'body' => $msg->body,
                    'message' => json_encode(json_decode($msg->body, true)['message']),
                    'consumed_at' => $time,
                    'consumed_datetime' => date('Y-m-d H:i:s', $time)

                ]);
            }, 
            $this->queueName,
            $this->consumerTag,
            false
        );
    }
}
