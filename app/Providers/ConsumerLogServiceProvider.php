<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Database\ConnectionResolverInterface;

use App\EventBus\RabbitMQ;

class ConsumerLogServiceProvider {
    /**
     * The connection resolver implementation.
     *
     * @var \Illuminate\Database\ConnectionResolverInterface
     */
    protected $resolver;

    /**
     * The database connection name.
     *
     * @var string
     */
    protected $database;

    /**
     * The database table.
     *
     * @var string
     */
    protected $table;

    /**
     * The log mode. Available Settings: "single", "monthly", "daily"
     * 
     * @var string
     */
    protected $log;

    /**
     * Create a new database publisher log provider.
     *
     * @param  \Illuminate\Database\ConnectionResolverInterface  $resolver
     * @param  string  $database
     * @param  string  $table
     * @param  string  $log
     * @return void
     */
    public function __construct(ConnectionResolverInterface $resolver, $database, $table, $log)
    {
        $this->table = $table;
        $this->resolver = $resolver;
        $this->database = $database;
        $this->log = $log;
    }

    public function info($consume) {
        $source_from = array_get($consume, 'body.source_from');
        $transaction_id = array_get($consume, 'body.transaction_id');
        $token = array_get($consume, 'body.token');
        $routing_key = array_get($consume, 'routing_key');
        $binding_key = array_get($consume, 'binding_key');
        $queue_name = array_get($consume, 'queue_name');
        $tag = array_get($consume, 'tag');
        
        $delivery = array_get($consume, 'body');

        return [
            $source_from,
            $transaction_id,
            $routing_key,
            $binding_key,
            $queue_name,
            $tag,
            $delivery,
            $token
        ];
    }

    /**
     * Log a consumed message into storage.
     *
     * @param  string  $binding_key
     * @param  mixed   $consume
     * @return int|null
     */
    public function log($binding_key, $consume)
    {
        $logged_at = Carbon::now()->getTimestamp();
        $logged_datetime = date('Y-m-d H:i:s', $logged_at);
        $processed = false;

        list(
            $source_from,
            $transaction_id,
            $routing_key,
            $binding_key,
            $queue_name,
            $tag,
            $decodedDelivery
        ) = $this->info($consume);

        $delivery = json_encode($decodedDelivery);
        
        return $this->getTable(array_get($decodedDelivery, 'published_at', time()))
                    ->insertGetId(compact(
                        'source_from', 
                        'transaction_id', 
                        'routing_key',
                        'binding_key', 
                        'queue_name',
                        'tag',
                        'delivery', 
                        'processed', 
                        'logged_at',
                        'logged_datetime'
                    ));
    }

    /**
     * Send an ack signal to publishers.
     *
     * @param  mixed   $consume
     * @return int|null
     */
    public function sendAck($consume) {
        list(
            $source_from,
            $transaction_id,
            $routing_key,
            ,
            ,
            ,
            $delivery
        ) = $this->info($consume);

        $published_at = array_get($delivery, 'published_at');

        (new RabbitMQ())->publish('readmsg', ['message' => compact('source_from', 'transaction_id', 'routing_key', 'published_at')])
                        ->destroy();
    }

    /**
     * Mark the consume processed.
     *
     * @param  mixed   $consume
     * @return int|null
     */
    public function processed($consume) {
        list(
            $source_from,
            $transaction_id,
            $routing_key,
            $binding_key,
            $queue_name,
            $tag,
            $delivery
        ) = $this->info($consume);

        $processed = true;
        $processed_at = time();
        $processed_datetime = date('Y-m-d H:i:s', $processed_at);

        $this->getTable(array_get($delivery, 'published_at', time()))
             ->where('source_from', $source_from)
             ->where('transaction_id', $transaction_id)
             ->where('routing_key', $routing_key)
             ->where('binding_key', $binding_key)
             ->where('queue_name', $queue_name)
             ->where('tag', $tag)
             ->update(compact('processed', 'processed_at', 'processed_datetime'));
    }

    /**
     * Get a new query builder instance for the table.
     *
     * @param int $time
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getTable(int $time)
    {        
        return $this->resolver
                    ->connection($this->database)
                    ->table($this->buildTable($time));
    }

    /**
     * Build the table name based on the log method.
     *
     * @param int $time
     * @return string
     */
    protected function buildTable(int $time) : string {
        if ($this->log === 'monthly') {
            return $this->table . '_' . date('Ym', $time);
        }

        if ($this->log === 'daily') {
            return $this->table . '_' . date('Ymd', $time);
        }

        return $this->table;
    }
}