<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Queue\Failed\DatabaseFailedJobProvider;

class MongoFailedJobExtentProvider extends DatabaseFailedJobProvider
{
    /**
     * Log a failed job into storage.
     *
     * @param  string $connection
     * @param  string $queue
     * @param  string $payload
     * @param  string $transaction_id
     * @param  \Exception  $exception
     *
     * @return void
     */
    public function log($connection, $queue, $payload, $exception, $transaction_id = '')
    {
        $failed_at = Carbon::now()->getTimestamp();

        $exception = (string) $exception;

        return $this->getTable()->insertGetId(compact(
            'connection', 'transaction_id', 'queue', 'payload', 'exception', 'failed_at'
        ));
    }
    
    public function all()
    {
        $failedJobs = $this->getTable()->orderBy('_id', 'asc')->get()->all();
        
        $failedJobs = array_map(function($job) {
            $job['id'] = strval($job['_id']);
            return $job;
        }, $failedJobs);

        return $failedJobs;
    }

    /**
     * Get a single failed job.
     *
     * @param  mixed  $id
     * @return array
     */
    public function find($id)
    {
        $job = $this->getTable()->find($id);

        $job['id'] = strval($job['_id']);

        return (object)$job;
    }

    /**
     * Delete a single failed job from storage.
     *
     * @param  mixed  $id
     * @return bool
     */
    public function forget($id)
    {
        return $this->getTable()->where('_id', $id)->delete() > 0;
    }

}
