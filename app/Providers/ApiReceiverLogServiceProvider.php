<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Database\ConnectionResolverInterface;

use App\EventBus\RabbitMQ;

class ApiReceiverLogServiceProvider {
    const MAX_SIZE_RESPONSE = 65535;

    /**
     * The connection resolver implementation.
     *
     * @var \Illuminate\Database\ConnectionResolverInterface
     */
    protected $resolver;

    /**
     * The database connection name.
     *
     * @var string
     */
    protected $database;

    /**
     * The database table.
     *
     * @var string
     */
    protected $table;

    /**
     * The log mode. Available Settings: "single", "monthly", "daily"
     * 
     * @var string
     */
    protected $log;

    /**
     * Create a new database api log provider.
     *
     * @param  \Illuminate\Database\ConnectionResolverInterface  $resolver
     * @param  string  $database
     * @param  string  $table
     * @param  string  $log
     * @return void
     */
    public function __construct(ConnectionResolverInterface $resolver, $database, $table, $log)
    {
        $this->table = $table;
        $this->resolver = $resolver;
        $this->database = $database;
        $this->log = $log;
    }

    /**
     * Log received request into storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $response
     * @param bool $async
     * @return int|null
     */
    public function log(\Illuminate\Http\Request $request, $response, $async = false) {
        $logged_at = Carbon::now()->getTimestamp();
        $logged_datetime = date('Y-m-d H:i:s', $logged_at);

        $transaction_id = app('transaction')->get();

        try {
            $claims = app('api.token')->getClaims($request->header('Token'));
            $source_from = array_get($claims, 'iss');
            $request_at = array_get($claims, 'iat');
        } 
        catch(\Exception $ex) {
            $source_from = str_slug(env('APP_NAME', 'laravel'), '_');
            $request_at = time();
        }

        $response_content = $response->content();
        $response_length = strlen($response_content);

        $request = [
            'url'       => $request->url(),
            'method'    => $request->method(),
            'data'      => json_encode($request->input()),
            'headers'   => $request->header(),
        ];

        $response = [
            // 'headers' => $response->header(),
            'status_code' => $response->status()
        ];

        if ($response_length <= self::MAX_SIZE_RESPONSE) {
            $response['body'] = $response_content;
        } else {
            $response['body'] = str_limit($response_content, self::MAX_SIZE_RESPONSE, ' (...)');
        }

        return $this->getTable($request_at)
                    ->insertGetId(compact(
                        'source_from',
                        'transaction_id',
                        'async',
                        'request',
                        'response',
                        'logged_at',
                        'logged_datetime'
                    ));
    }

    /**
     * Get a new query builder instance for the table.
     *
     * @param int $time
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getTable(int $time)
    {        
        return $this->resolver
                    ->connection($this->database)
                    ->table($this->buildTable($time));
    }

    /**
     * Build the table name based on the log method.
     *
     * @param int $time
     * @return string
     */
    protected function buildTable(int $time) : string {
        if ($this->log === 'monthly') {
            return $this->table . '_' . date('Ym', $time);
        }

        if ($this->log === 'daily') {
            return $this->table . '_' . date('Ymd', $time);
        }

        return $this->table;
    }
}