<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Providers\ApiRequesterLogServiceProvider;
use App\Providers\ApiReceiverLogServiceProvider;
use App\Api\RESTful;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register api services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('api.restful', function($app) {
            return new RESTful();
        });

        $this->app->singleton('api.log.requester', function ($app) {
            return new ApiRequesterLogServiceProvider(
                $app['db'],
                config('api.log_requester.database'), 
                config('api.log_requester.table'),
                config('api.log_requester.log')
            );
        });

        $this->app->singleton('api.log.receiver', function ($app) {
            return new ApiReceiverLogServiceProvider(
                $app['db'],
                config('api.log_receiver.database'), 
                config('api.log_receiver.table'),
                config('api.log_receiver.log')
            );
        });

        $this->app->singleton('api.token', function ($app) {
            return new \Leo\JWT\JWT(config('api'));
        });
        
    }
}
