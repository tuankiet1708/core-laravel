<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Database\ConnectionResolverInterface;

class PublisherLogServiceProvider {
    /**
     * The connection resolver implementation.
     *
     * @var \Illuminate\Database\ConnectionResolverInterface
     */
    protected $resolver;

    /**
     * The database connection name.
     *
     * @var string
     */
    protected $database;

    /**
     * The database table.
     *
     * @var string
     */
    protected $table;

    /**
     * The log mode. Available Settings: "single", "monthly", "daily"
     * 
     * @var string
     */
    protected $log;

    /**
     * Create a new database publisher log provider.
     *
     * @param  \Illuminate\Database\ConnectionResolverInterface  $resolver
     * @param  string  $database
     * @param  string  $table
     * @param  string  $log
     * @return void
     */
    public function __construct(ConnectionResolverInterface $resolver, $database, $table, $log)
    {
        $this->table = $table;
        $this->resolver = $resolver;
        $this->database = $database;
        $this->log = $log;
    }

    /**
     * Log a published message into storage.
     *
     * @param  string  $routing_key
     * @param  mixed  $decodedDelivery
     * @return int|null
     */
    public function log($routing_key, $decodedDelivery)
    {
        $logged_at = Carbon::now()->getTimestamp();
        $logged_datetime = date('Y-m-d H:i:s', $logged_at);
        $ack = false;

        $source_from = array_get($decodedDelivery, 'source_from');
        $transaction_id = array_get($decodedDelivery, 'transaction_id');

        $delivery = json_encode($decodedDelivery);

        return $this->getTable(array_get($decodedDelivery, 'published_at', time()))
                    ->insertGetId(compact(
                        'source_from', 
                        'transaction_id', 
                        'routing_key', 
                        'delivery', 
                        'ack', 
                        'logged_at',
                        'logged_datetime'
                    ));
    }

    /**
     * Mark the log acknowledged.
     *
     * @param  mixed  $trace
     * @param  mixed  $consumer
     * @return int|null
     */
    public function ack($trace, $consumer) {
        // $this->getTable(array_get($trace, 'published_at'))
        //      ->where(array_only($trace, ['source_from', 'transaction_id', 'routing_key']))
        //      ->push(
        //         'next', 
        //         array_only($consumer, ['source_from', 'transaction_id', 'routing_key']) + [
        //             'ack_at' => time(),
        //             'ack_datetime' => date('Y-m-d H:i:s', time())
        //         ]
        //      );

        $result =  $this->getTable(array_get($trace, 'published_at'))
                        ->raw(function ($clt) use ($trace, $consumer) {
                            $time = time();

                            return $clt->updateOne(
                                array_only($trace, ['source_from', 'transaction_id', 'routing_key']),
                                [
                                    '$push' => [
                                        'next' => array_only($consumer, ['source_from', 'transaction_id', 'routing_key']) + [
                                            'ack_at' => $time,
                                            'ack_datetime' => date('Y-m-d H:i:s', $time)
                                        ]
                                    ],
                                    '$set' => [
                                        'ack' => true,
                                        'ack_at' => $time,
                                        'ack_datetime' => date('Y-m-d H:i:s', $time)
                                    ]
                                ]
                            )->getModifiedCount();
                    });

        return $result ? true : false;
    }

    /**
     * Get a new query builder instance for the table.
     *
     * @param int $time
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getTable(int $time)
    {        
        return $this->resolver
                    ->connection($this->database)
                    ->table($this->buildTable($time));
    }

    /**
     * Build the table name based on the log method.
     *
     * @param int $time
     * @return string
     */
    protected function buildTable(int $time) : string {
        if ($this->log === 'monthly') {
            return $this->table . '_' . date('Ym', $time);
        }

        if ($this->log === 'daily') {
            return $this->table . '_' . date('Ymd', $time);
        }

        return $this->table;
    }
}