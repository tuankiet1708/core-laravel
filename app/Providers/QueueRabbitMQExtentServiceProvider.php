<?php

namespace App\Providers;

use VladimirYuldashev\LaravelQueueRabbitMQ\Queue\Connectors\RabbitMQConnector;
use VladimirYuldashev\LaravelQueueRabbitMQ\LaravelQueueRabbitMQServiceProvider;
use App\Providers\MongoFailedJobExtentProvider;

class QueueRabbitMQExtentServiceProvider extends LaravelQueueRabbitMQServiceProvider
{
    /**
     * Register the failed job services for mongodb.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * @var \Illuminate\Queue\QueueManager
         */
        $manager = $this->app['queue'];
        $manager->addConnector('rabbitmq', function () {
            return new RabbitMQConnector;
        });

        $this->app->singleton('queue.failer', function ($app) {
            return new MongoFailedJobExtentProvider($app['db'], config('queue.failed.database'), config('queue.failed.table'));
        });
    }

}
