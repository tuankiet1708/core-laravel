<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Providers\TransactionServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton('transaction', function ($app) {
            return new TransactionServiceProvider();
        });
    }
}
