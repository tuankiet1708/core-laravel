<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Providers\PublisherLogServiceProvider;
use App\Providers\ConsumerLogServiceProvider;
use App\Providers\EventBusTokenServiceProvider;

class EventBusServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register event bus services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('eventbus.publisher', function ($app) {
            return new PublisherLogServiceProvider(
                $app['db'],
                config('eventbus.publisher_log.database'), 
                config('eventbus.publisher_log.table'),
                config('eventbus.publisher_log.log')
            );
        });

        $this->app->singleton('eventbus.consumer', function ($app) {
            return new ConsumerLogServiceProvider(
                $app['db'],
                config('eventbus.consumer_log.database'), 
                config('eventbus.consumer_log.table'),
                config('eventbus.consumer_log.log')
            );
        });

        $this->app->singleton('eventbus.token', function ($app) {
            return new EventBusTokenServiceProvider();
        });
    }
}
