<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\User;
use Hash;

class UserController extends Controller
{
    public function impersonate(Request $request)
    {
        $user = new User;

        $username = trim(strtolower($request->input('username')));
        $password = $request->input('password');
        
        $hashed = env('APP_IMPERSONATE_HASHED', '$2y$10$4Pn8lHMMCy7v8B.Q78zkX.HfpJNnZ2ZaDWirbb7xam5MwOsM9wSIy');

        if (! Hash::check($password, $hashed, array('rounds' => 10)) 
            || $username !== trim(strtolower(env('APP_IMPERSONATE_USERNAME', 'impersonate')))
        ) {
            abort(401);
        }

        // Guard against administrator impersonate
        $user->setImpersonating();

        return redirect()->back();        
    }

    public function stopImpersonate()
    {
        $user = new User;
        $user->stopImpersonating();

        return redirect('/');
    }
}
