<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ApiToken
{
    /**
     * Handle an incoming request.
     */
    public function handle($request, Closure $next)
    {
        app('api.token')->checkValid(strval($request->header('Token')));

        return $next($request);
    }
}