<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Impersonate
{
    /**
     * Handle an incoming request.
     */
    public function handle($request, Closure $next)
    {
        if (! $request->session()->has('impersonate'))
        {
            return abort(401);
        }

        return $next($request);
    }
}