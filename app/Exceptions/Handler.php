<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response as IlluminateResponse;
use Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    protected function responseWithJsonException($request, Exception $exception) {
        Log::error('ExceptionHandler', [
            'transaction_id' => app('transaction')->get(),
            'exception_class' => get_class($exception),
            'request' => $request->input(),
            'code' => $exception->getCode(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'message' => $exception->getMessage(),
            'trace_as_string' => $exception->getTraceAsString()
        ]);

        list($statusCode, $errorMessage, $validations) = $this->detectExceptionAndReturnCodeAndMessage($exception);

        return response()->json([
            'transaction_id' => app('transaction')->get(),
            'data' => $validations,
            'status' => $statusCode,
            'status_code' => $statusCode,
            'errors' => [
                [
                    'code' => $statusCode,
                    'message' => $errorMessage,
                    'validations' => $validations
                ]
            ]
        ], $statusCode, [/* headers */]);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if (env('APP_DEBUG')) {
            return parent::render($request, $exception);
        }
        else {
            return $this->responseWithJsonException($request, $exception);
        }
    }

    protected function detectExceptionAndReturnCodeAndMessage(Exception $exception) {
        // default
        $statusCode = IlluminateResponse::HTTP_BAD_REQUEST;
        $errorMessage = 'An error occurred';

        // NotFoundHttpException
        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            $statusCode = IlluminateResponse::HTTP_NOT_FOUND;
            $errorMessage = 'http_not_found';
        }

        // MongoException
        elseif ($exception instanceof \MongoException
             || $exception instanceof \MongoConnectionException
        ) {
            $statusCode = IlluminateResponse::HTTP_SERVICE_UNAVAILABLE;
            $errorMessage = 'mongo_exception';
        }

        // TokenMismatchException
        elseif ($exception instanceof TokenMismatchException) {
            $statusCode = IlluminateResponse::HTTP_UNAUTHORIZED;
            $errorMessage = 'token_mismatch';
        }

        // TokenBlacklisted
        elseif ($exception instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException
             && $exception->getMessage() == 'Token not provided'
        ) {
            $statusCode = IlluminateResponse::HTTP_UNAUTHORIZED;
            $errorMessage = 'token_not_provided';
        }

        // TokenBlacklisted
        elseif ($exception instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException
             && $exception->getMessage() == 'The token has been blacklisted'
        ) {
            $statusCode = IlluminateResponse::HTTP_UNAUTHORIZED;
            $errorMessage = 'token_blacklisted';
        }

        // TokenExpired
        elseif ($exception instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException
             && $exception->getMessage() == 'Token has expired'
        ) {
            $statusCode = IlluminateResponse::HTTP_UNAUTHORIZED;
            $errorMessage = 'token_expired';
        }

        // InvalidInputException
        elseif ($exception instanceof \Lucid\Foundation\InvalidInputException
             || $exception instanceof \LucidApp\Foundation\InvalidInputException
             || $exception instanceof \Illuminate\Validation\ValidationException
        ) {
            $statusCode = IlluminateResponse::HTTP_PRECONDITION_FAILED;
            $errorMessage = $exception->getMessage();
            $validations = $exception->validations ?? null;
        }

        // FatalThrowableError
        elseif ($exception instanceof \Symfony\Component\Debug\Exception\FatalThrowableError) {
            $errorMessage = 'Syntax error';
        }

        // Default
        elseif (!empty($exception->getCode())
             && !empty($exception->getMessage())
        ) {
            $statusCode = $exception->getCode();
            $errorMessage = $exception->getMessage();
        }

        elseif (!empty($exception->getMessage())) {
            $errorMessage = $exception->getMessage();
        }


        return [
            $statusCode,
            $errorMessage,
            $validations ?? null
        ];
    }
}
