<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;
use Carbon\Carbon;
use App\Jobs\Base\BaseJob;

class ExampleJob extends BaseJob
{
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 30;

    /**
     * The number of times the job may be attempted.
     * If the maximum number of attempts is specified on the job, it will take precedence over the value provided on the command line:
     * @var int
     */
    public $tries = 1; // DO NOT put 0 here -> it means trying forever


    public $content;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $content = "It's an example job.")
    {
        //
        $this->chainQueue = $this->queue = 'worker_core_example';
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Note: Please include this line always on top to capture the transaction id 
        parent::handle(); 
        // ==============

        (new \App\Jobs\Base\PublishAMessageJob('log.error', [
            'key.subKey' => 'This is an error log - called by SafeJob.',
            'transactionId' => app('transaction')->get()
        ]))->handle();

        app('api.restful')->requestService('log_app', '/api', 'POST', ['key.subKey' => 'Hello World'], 'GET');
    }
}
