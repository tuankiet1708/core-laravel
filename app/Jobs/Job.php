<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use Log;
use DB;
use App\Jobs\Base\BaseJob;

class Job extends BaseJob
{
    public $content; 

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $content = "It's a blank job.")
    {
        //
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Note: Please include this line always on top to capture the transaction id 
        parent::handle(); 
        // ==============

        //
        DB::collection('test_queue')->insert([
            'content' => $this->content,
            'run_at' => Carbon::now()->getTimestamp()
        ]);
    }
}
