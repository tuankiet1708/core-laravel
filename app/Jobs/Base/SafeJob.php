<?php

namespace App\Jobs\Base;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use Log;
use DB;

use App\Jobs\Base\FailedJobLogger;

class SafeJob
{
    protected $job;
    protected $queue;
    protected $transactionId;

    /**
     * Create a new SafeJob instance.
     *
     * @return void
     */
    public function __construct(object $job, string $queue = null)
    {
        //
        $this->job = $job;
        $this->queue = $queue;

        if (!empty($this->queue)) {
            $this->job = $this->job->onQueue($this->queue);
        } else {
            $this->queue = $this->job->queue;
        }

        if (empty(app('transaction')->get())) {
            app('transaction')->generate();
        }

        $this->transactionId = app('transaction')->get();
    }

    /**
     * Dispatch the job.
     *
     * @return void
     */
    public function dispatch()
    {
        $this->job->transactionId = $this->transactionId;
        
        try {
            dispatch($this->job);
        } catch (\Exception $ex) {
            FailedJobLogger::logFailedJob(
                $this->job, 
                $this->queue, 
                $ex->getMessage(),
                $this->transactionId
            );
        }
    }
}
