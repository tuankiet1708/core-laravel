<?php

namespace App\Jobs\Base;

trait QueueWithTransaction
{
    /**
     * The transaction id of the job.
     *
     * @var string|null
     */
    public $transactionId;
}
