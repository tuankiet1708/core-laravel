<?php

namespace App\Jobs\Base;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;
use Carbon\Carbon;

use App\EventBus\RabbitMQ;
use App\Libraries\Uuid;
use App\Jobs\Base\BaseJob;

class PublishAMessageJob extends BaseJob
{
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 30;

    /**
     * The number of times the job may be attempted.
     * If the maximum number of attempts is specified on the job, it will take precedence over the value provided on the command line:
     * @var int
     */
    public $tries = 1; // DO NOT put 0 here -> it means trying forever


    public $message;
    public $routingKey;

    /**
     * Tracking time
     */
    public $dispatchedAt;
    public $dispatchedDatetime;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $routingKey, $message)
    {
        //
        $this->chainQueue = $this->queue = 'worker_core_publisher';
        $this->message = $message;
        $this->routingKey = $routingKey;

        $this->dispatchedAt = time();
        $this->dispatchedDatetime = date('Y-m-d H:i:s', $this->dispatchedAt);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Note: Please include this line always on top to capture the transaction id 
        parent::handle(); 
        // ==============
        
        $time = time();
        
        $delivery = [
            'published_at' => $time,
            'published_datetime' => date('Y-m-d H:i:s', $time),
            'dispatched_at' => $this->dispatchedAt,
            'dispatched_datetime' => $this->dispatchedDatetime,    
            
            'message' => $this->message
        ];

        (new RabbitMQ())->publish($this->routingKey, $delivery)
                        ->destroy();
    }
}
