<?php

namespace App\Jobs\Base;

/**
 * Class FailedJobLogger
 * @package App\Jobs\Base
 */
class FailedJobLogger
{
    static public function logFailedJob($job, $queue, $exception = null, $transaction_id = '') {
        $failer = app('queue.failer');

        $payload = json_encode([
            'job' => 'Illuminate\Queue\CallQueuedHandler@call',
            'data' => ['command' => serialize(clone $job)]
        ]);
        $failedId = $failer->log(config('queue.default'), $queue, $payload, $exception, $transaction_id);

        return [
            'failedId' => $failedId,
            'queue' => $job,
            'payload' => $payload,
            'failed' => true
        ];
    }
}


