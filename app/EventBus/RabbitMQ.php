<?php

namespace App\EventBus;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\Libraries\Uuid;

class RabbitMQ {
    private $config;

    protected $connection;
    protected $channel;

    protected static $sourceFrom;

    /**
     * Create a new eventbus instance.
     *
     * @return void
     */
    public function __construct() {
        $this->config = config('eventbus.rabbitmq');

        $this->connection = new AMQPStreamConnection(
            $this->config['host'],
            $this->config['port'],
            $this->config['login'],
            $this->config['password'],
            $this->config['vhost']
        );

        $this->channel = $this->connection->channel();
    }

    /**
     * Get channel.
     *
     */
    public function channel() {
        return $this->channel;
    }

    /**
     * Publish a message.
     *
     * @param string    $routingKey
     * @param mixed     $message
     * @param bool      $log
     * @return \App\EventBus\RabbitMQ
     */
    public function publish(string $routingKey, $message, bool $log = true) {
        if (empty($routingKey)) {
            throw new \Excpetion('Routing key can not be empty! More info at https://www.rabbitmq.com/tutorials/tutorial-five-php.html.');
        }
        
        // Declare 
        $this->channel->exchange_declare(
            $this->config['exchange_params']['name'], 
            $this->config['exchange_params']['type'], 
            $this->config['exchange_params']['passive'], 
            $this->config['exchange_params']['durable'], 
            $this->config['exchange_params']['auto_delete']
        );

        $this->transaction();

        // Add some traces
        $delivery = array_merge($message, [
            'source_from' => static::$sourceFrom,
            'transaction_id' => app('transaction')->get(),
            'token' => app('eventbus.token')->buildToken(),
        ]);

        // Publisher log
        if ($log) app('eventbus.publisher')->log($routingKey, $delivery);

        // Publish a message
        $this->channel->basic_publish(
            new AMQPMessage(json_encode($delivery)), 
            $this->config['exchange_params']['name'], 
            $routingKey
        );
        
        return $this;
    }

    /**
     * Consume a message.
     *
     * @param string    $bindingKey
     * @param callable  $callback
     * @param string    $queueName
     * @param string    $tag
     * @param bool      $log
     */
    public function consume(string $bindingKey, callable $callback, string $queueName, string $tag = '', bool $log = true) {
        if (empty($bindingKey)) {
            throw new \Excpetion('Binding key can not be empty! More info at https://www.rabbitmq.com/tutorials/tutorial-five-php.html.');
        }

        if (empty($queueName)) {
            throw new \Excpetion('Queue name for consumer can not be empty! More info at https://www.rabbitmq.com/tutorials/tutorial-five-php.html.');
        }

        list($queueName, ,) = $this->channel->queue_declare(
            $queueName, 
            $this->config['queue_params']['passive'], 
            $this->config['queue_params']['durable'], 
            $this->config['queue_params']['exclusive'], 
            $this->config['queue_params']['auto_delete']
        );

        $overridedCallback = function ($msg) use ($bindingKey, $callback, $queueName, $tag, $log) {
            $decoded = json_decode($msg->body, true);

            // Consumer log
            $consume = [
                'routing_key' => $msg->delivery_info['routing_key'],
                'binding_key' => $bindingKey,
                'queue_name' => $queueName,
                'tag' => $tag,
                'body' => $decoded,
            ];

            list( , $transaction_id, , , , , , $token) = app('eventbus.consumer')->info($consume);

            // Check token
            app('eventbus.token')->checkValid($token);

            // Transaction processing
            $this->transaction(compact('transaction_id'));
            
            if ($log) app('eventbus.consumer')->log($bindingKey, $consume);

            // Do something with callback
            $callback($msg);

            if ($log) {
                // Mark the consume processed
                app('eventbus.consumer')->processed($consume);
                // Send an ack signal to publisher
                app('eventbus.consumer')->sendAck($consume);
            }
        };

        // Bind an queue
        $this->channel->queue_bind($queueName, $this->config['exchange_params']['name'], $bindingKey);

        // Comsume an message
        $this->channel->basic_consume($queueName, $tag, false, true, false, false, $overridedCallback);

        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }

        return $this;
    }

    public function destroy() {
        $this->channel->close();
        $this->connection->close();

        return $this;
    }

    protected function transaction($consumeInfo = null) {
        $transactionId = array_get($consumeInfo, 'transaction_id', null);
        if (!empty($transactionId)) {
            app('transaction')->set($transactionId);
        }

        if (empty(app('transaction')->get())) {
            app('transaction')->generate();
        }
        if (empty(static::$sourceFrom)) {
            static::$sourceFrom = static::getSourceFrom();
        }
    }

    static public function genTransactionId(): string {
        return strval(Uuid::generate(4));
    }

    static public function getSourceFrom(): string {
        return str_slug(env('APP_NAME', 'laravel'), '_');
    }

    
}