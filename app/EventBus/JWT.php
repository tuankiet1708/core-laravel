<?php

namespace App\EventBus;

interface JWT {
    public function buildToken();
    public function parse(string $token);
    public function checkValid(string $token);
    public function info();
}