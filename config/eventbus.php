<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each server that
    | is used by your application. A default configuration has been added
    | for each back-end shipped with Laravel. You are free to add more.
    |
    */
        
    'rabbitmq' => [
        'driver'                => 'rabbitmq',
        'host'                  => env('RABBITMQ_HOST', '127.0.0.1'),
        'port'                  => env('RABBITMQ_PORT', 5672),
        'vhost'                 => env('RABBITMQ_VHOST', '/'),
        'login'                 => env('RABBITMQ_LOGIN', 'guest'),
        'password'              => env('RABBITMQ_PASSWORD', 'guest'),
        'queue'                 => env('RABBITMQ_QUEUE', 'default'), // name of the default queue,
        'exchange_declare'      => env('RABBITMQ_EXCHANGE_DECLARE', true), // create the exchange if not exists
        'queue_declare_bind'    => env('RABBITMQ_QUEUE_DECLARE_BIND', true), // create the queue if not exists and bind to the exchange
        'queue_params'          => [
            'passive'           => env('RABBITMQ_QUEUE_PASSIVE', false),
            'durable'           => env('RABBITMQ_QUEUE_DURABLE', true),
            'exclusive'         => env('RABBITMQ_QUEUE_EXCLUSIVE', false),
            'auto_delete'       => env('RABBITMQ_QUEUE_AUTODELETE', false),
        ],
        'exchange_params' => [
            'name'        => 'eventbus',
            'type'        => env('RABBITMQ_EXCHANGE_TYPE', 'topic'), // more info at http://www.rabbitmq.com/tutorials/amqp-concepts.html
            'passive'     => env('RABBITMQ_EXCHANGE_PASSIVE', false),
            'durable'     => env('RABBITMQ_EXCHANGE_DURABLE', true), // the exchange will survive server restarts
            'auto_delete' => env('RABBITMQ_EXCHANGE_AUTODELETE', false),
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "monthly", "daily"
    |
    */
    'publisher_log' => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table' => 'eventbus_publisher_log',
        'log' => 'monthly'
    ],

    'consumer_log' => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table' => 'eventbus_consumer_log',
        'log' => 'monthly'
    ],

    'jwt' => [
        /*
        |--------------------------------------------------------------------------
        | JWT App ID
        |--------------------------------------------------------------------------
        |
        | Configures the id (jti claim), replicating as a header item
        |
        */
        'app_id' => env('JWT_EVENTBUS_APP_ID', 'laravel'),

        /*
        |--------------------------------------------------------------------------
        | JWT Authentication Secret
        |--------------------------------------------------------------------------
        |
        | Don't forget to set this in your .env file, as it will be used to sign
        | your tokens. A helper command is provided for this:
        | `php artisan jwt:secret -s`
        |
        */
        'secret' => env('JWT_EVENTBUS_SECRET', 'secret_key'),

        /*
        |--------------------------------------------------------------------------
        | JWT time to live
        |--------------------------------------------------------------------------
        |
        | Specify the length of time (in seconds) that the token will be valid for.
        | Defaults to 5 minutes.
        |
        | You can also set this to null, to yield a never expiring token.
        | Some people may want this behaviour for e.g. a mobile app.
        | This is not particularly recommended, so make sure you have appropriate
        | systems in place to revoke the token if necessary.
        |
        */

        'ttl' => env('JWT_EVENTBUS_TTL', 300),

        /*
        |--------------------------------------------------------------------------
        | JWT Authentication Secrets of Parties
        |--------------------------------------------------------------------------
        |
        | Specify the secrets of parties for jwt token validation
        |
        */
        'parties' => array_key_by(json_decode(env('JWT_EVENTBUS_PARTIES', '[]'), true), '0'),
    ]
];