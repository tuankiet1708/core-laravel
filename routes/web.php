<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/example', function () {
    // Example of pushing a job into queue
    (new \App\Jobs\Base\SafeJob(
        new \App\Jobs\ExampleJob()
    ))->dispatch();

    return 'done.';
});

// http://blog.mauriziobonani.com/easily-impersonate-any-user-in-a-laravel-application/
Route::group([
    'prefix' => '/users'
], function() {
    Route::match(['get', 'post'], '/impersonate', 'UserController@impersonate');
    Route::get('/stop-impersonate', 'UserController@stopImpersonate');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
