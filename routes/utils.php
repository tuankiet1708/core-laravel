<?php

Route::group([
    'prefix' => '/utils',
], function() {
    Route::get('/', function() {
        return view('utils');
    })->name('utils');

    // Compare raw password and hashed password
    // URL: /utils/check-password?password=<password>&hashed=<hashed_password>
    Route::match(['get', 'post'], '/check-password', function(\Illuminate\Http\Request $request) {
        $raw = $request->input('password');
        $hashed = $request->input('hashed');
        return response()->json([
            'result' => Hash::check($raw, $hashed, array('rounds' => 10)),
        ]);
    });

    // Generate hashed password
    // URL: /utils/password?password=<password>
    Route::match(['get', 'post'], '/password', function(\Illuminate\Http\Request $request) {
        $raw = $request->input('password');
        $hashed = Hash::make($raw, array('rounds' => 10));
        return response()->json([
            'raw' => $raw,
            'hashed' => $hashed,
            'result' => Hash::check($raw, $hashed, array('rounds' => 10))
        ]);
    });

    // Generate JWT Secret
    // URL: /utils/jwt
    Route::match(['get', 'post'], '/jwt', function(\Illuminate\Http\Request $request) {
        $stream = fopen("php://output", "w");

        \Illuminate\Support\Facades\Artisan::call('jwt:secret', ['-s' => 'show'], new \Symfony\Component\Console\Output\StreamOutput($stream));
        
        fclose($stream);
    });

    // Queue Utils
    Route::group(['prefix' => 'queue', 'middleware' => ['impersonate']], function() {
        Route::get('/retry/{id?}', function($id = null) {
            $stream = fopen("php://output", "w");

            \Illuminate\Support\Facades\Artisan::call('queue:retry', ['id' => [$id ? $id : 'all']], new \Symfony\Component\Console\Output\StreamOutput($stream));
            
            fclose($stream);
        });
        Route::get('/failed', function() {
            $failedJobs = DB::collection(config('queue.failed.table'))->get()->toArray();

            return response()->json(['total' => count($failedJobs), 'failed' => $failedJobs, 'collection' => config('queue.failed.table')]);
        });
        Route::get('/flush', function() {
            $stream = fopen("php://output", "w");

            \Illuminate\Support\Facades\Artisan::call('queue:flush', [], new \Symfony\Component\Console\Output\StreamOutput($stream));
            
            fclose($stream);
        });
        Route::get('/forget/{id}', function($id) {
            $stream = fopen("php://output", "w");

            \Illuminate\Support\Facades\Artisan::call('queue:forget', ['id' => $id], new \Symfony\Component\Console\Output\StreamOutput($stream));
            
            fclose($stream);
        });
    });

    Route::group([
        'prefix' => '/logs', 
        'middleware' => ['impersonate']
    ], function() {
        // Route::get('/logs', function() {
        //     return response()->json($logs = LogReader::get());
        // });
        
        Route::get('/', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    Route::group(['prefix' => 'status', 'middleware' => ['impersonate']], function() {
        Route::get('/{status}', function($status) {
            $stream = fopen("php://output", "w");

            \Illuminate\Support\Facades\Artisan::call("$status", [], new \Symfony\Component\Console\Output\StreamOutput($stream));
            
            fclose($stream);
        });
    });
});