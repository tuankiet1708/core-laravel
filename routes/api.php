<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => '/', 'middleware' => ['cors']], function() {
    Route::match(['get', 'post', 'put', 'delete'], '/', function(\Illuminate\Http\Request $request) {
        return response()->json([
            'params' => $request->input(),
            'headers' => $request->header()
        ]);
    });
});
