<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__.'/../')
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Custom The Application Monolog
|--------------------------------------------------------------------------
|
| Configuration daily monolog.
|
*/

$app->configureMonologUsing(function (\Monolog\Logger $logger) {
    $formatter = new \Monolog\Formatter\LineFormatter(null, null, true, true);

    // $maxFiles = 0;
    // $handler = new \Monolog\Handler\RotatingFileHandler(storage_path('logs/error.log'), $maxFiles, \Monolog\Logger::ERROR);
    // $handler->setFilenameFormat('{filename}-{date}', 'Y-m-d');
    // $handler->setFormatter($formatter);
    // $logger->pushHandler($handler);

    $bubble = false;
    $date = date('Y-m-d', time());

    // stream levels
    $streamLevels = [
        'debug' => \Monolog\Logger::DEBUG,
        'info' => \Monolog\Logger::INFO,
        'notice' => \Monolog\Logger::NOTICE,
        'warning' => \Monolog\Logger::WARNING,
        'error' => \Monolog\Logger::ERROR,
        'critical' => \Monolog\Logger::CRITICAL,
        'alert' => \Monolog\Logger::ALERT,
        'emergency' => \Monolog\Logger::EMERGENCY
    ];

    // file levels
    $fileLevels = [
        "logs/application-{$date}.log" => ['debug', 'info', 'notice', 'warning'],
        "logs/error-{$date}.log" => ['error', 'critical', 'alert', 'emergency'],
    ];

    // push handler
    foreach ($fileLevels as $file => $levels) {
        foreach ($levels as $lv) {
            $streamHandler = new \Monolog\Handler\StreamHandler(
                storage_path($file),
                $streamLevels[$lv],
                $bubble
            );
            $streamHandler->setFormatter($formatter);
            $logger->pushHandler($streamHandler);
        }
    }

    return $logger;
});

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
